package entrysystem.ui;

/**
 * Console menu for data, extended to also print table of data.
 */
abstract class ConsoleMenuData extends ConsoleMenu {
	protected void print() {
		printClear();
		printData();
		printMenu();
	}

	/**
	 * Print data table.
	 */
	protected abstract void printData();
}

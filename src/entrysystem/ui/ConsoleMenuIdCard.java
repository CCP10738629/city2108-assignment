package entrysystem.ui;

import entrysystem.data.IdCardFile;

/**
 * Menu for ID cards.
 */
final class ConsoleMenuIdCard extends ConsoleMenuData {

	@Override
	protected void printData() {
		IdCardFile.getInstance().print();
	}

	@Override
	protected String getTitle() {
		return "ID Cards";
	}

	@Override
	protected ConsoleMenuChoice[] getChoiceArray() {
		return new ConsoleMenuChoice[] {
				new ConsoleMenuChoice("1", "Add an ID card"),
				new ConsoleMenuChoice("2", "Edit or delete an ID card"),
				new ConsoleMenuChoice("3", "Save ID card data"),
				new ConsoleMenuChoice("4", "Reload ID card file")
		};
	}

	@Override
	protected ConsoleMenuChoice getExitChoice() {
		return new ConsoleMenuChoice("0 or \"back\"", "Go back to the main menu");
	}

	@Override
	protected void process(String input) {
		switch (input) {
			case "0":
			case "back":
				Console.setActiveMenu(new ConsoleMenuMain());
				break;
			case "1":
				IdCardFile.getInstance().addViaConsole();
				break;
			case "2":
				Console.setActiveMenu(new ConsoleMenuIdCardEdit());
				break;
			case "3":
				IdCardFile.getInstance().save();
				break;
			case "4":
				IdCardFile.getInstance().load();
				break;
		}
	}
}

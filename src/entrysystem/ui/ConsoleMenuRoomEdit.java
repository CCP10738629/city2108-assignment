package entrysystem.ui;

import entrysystem.data.*;
import entrysystem.pref.PreferenceKey;
import entrysystem.pref.Preferences;

/**
 * Menu for editing a room.
 */
public class ConsoleMenuRoomEdit extends ConsoleMenuDataEdit {

	private Room chosen;

	@Override
	protected void run() {
		print();
		if (chosen != null)
			process(getInput().toLowerCase());
	}

	@Override
	protected String getTitle() {
		return "Edit Room";
	}

	@Override
	protected ConsoleMenuChoice[] getChoiceArray() {
		return new ConsoleMenuChoice[] {
				new ConsoleMenuChoice("1", "change the name"),
				new ConsoleMenuChoice("2", "change the room type"),
				new ConsoleMenuChoice("3", "delete the room")
		};
	}

	@Override
	protected ConsoleMenuChoice getExitChoice() {
		return new ConsoleMenuChoice("0 or \"back\"", "go back to the rooms menu");
	}

	@Override
	protected void process(String input) {
		switch (input) {
			case "1":
				System.out.print("Enter a name or type 'cancel': ");
				String name = getInput();
				if (name.equalsIgnoreCase("cancel"))
					break;
				chosen.setName(name);
				if (Preferences.getInstance().getValue(PreferenceKey.bSaveDataOnChange).equals("true"))
					RoomFile.getInstance().save();
				break;
			case "3":
				System.out.println("Confirm room deletion (Y/N)");
				if (getInput().equalsIgnoreCase("y"))
					RoomFile.getInstance().remove(chosen.getId());
				else
					Console.messageBuffer.append("Deletion cancelled.");
				break;
			case "2":
				RoomType.print();
				boolean typeSet = false;
				while (!typeSet) {
					System.out.print("Enter an integer to choose a room type, or type 'cancel': ");
					String type = getInput();
					switch (type) {
						default:
							if (type.equalsIgnoreCase("cancel")) {
								typeSet = true;
								break;
							}
							System.out.println("Invalid input.\n");
							break;
						case "0":
						case "1":
						case "2":
						case "3":
						case "4":
							chosen.setType(RoomType.fromIntString(type));
							typeSet = true;
							if (Boolean.parseBoolean(Preferences.getInstance().getValue(PreferenceKey.bSaveDataOnChange)))
								IdCardFile.getInstance().save();
							break;
					}
				}
				break;
			case "0":
			case "back":
				Console.setActiveMenu(new ConsoleMenuRoom());
				break;
		}
	}

	@Override
	protected void printData() {
		chosen.printForMenu();
	}

	@Override
	protected boolean selectData() { // TODO refactor

		if (chosen != null)
			return true;

		System.out.print("Type 'cancel' to return to the previous menu.\n\n");

		while (true) {

			System.out.print("Enter an ID: ");
			String input = getInput();

			if (input.equalsIgnoreCase("cancel")) {
				Console.setActiveMenu(new ConsoleMenuRoom());
				return false;
			}

			try {
				if ((chosen = (Room) RoomFile.getInstance().get(Integer.parseInt(input))) == null) {
					System.out.println("Not a valid ID.\n");
				} else
					return true;
			} catch (NumberFormatException e) {
				System.out.println("Invalid input.\n");
			}
		}
	}
}

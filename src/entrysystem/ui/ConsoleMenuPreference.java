package entrysystem.ui;

import entrysystem.pref.PreferenceKey;
import entrysystem.pref.Preferences;

/**
 * Preference menu.
 */
final class ConsoleMenuPreference extends ConsoleMenu {

	@Override
	protected String getTitle() {
		return "Preferences Menu";
	}

	@Override
	protected ConsoleMenuChoice[] getChoiceArray() {
		return new ConsoleMenuChoice[] {
				new ConsoleMenuChoice("1", "enable GUI mode"),
				new ConsoleMenuChoice("2", "save data on change (currently " + Preferences.getInstance().getValue(PreferenceKey.bSaveDataOnChange) + ")"),
				new ConsoleMenuChoice("3", "save changes")
		};
	}

	@Override
	protected ConsoleMenuChoice getExitChoice() {
		return new ConsoleMenuChoice("0 or \"back\"", "go back to the main menu");
	}

	@Override
	protected void process(String input) {
		switch (input) {
			case "0":
			case "back":
				Console.setActiveMenu(new ConsoleMenuMain());
				break;
			case "1":
				UI.setActiveUI(new Gui());
				break;
			case "2":
				switch (Preferences.getInstance().getValue(PreferenceKey.bSaveDataOnChange)) {
					case "true":
						Preferences.getInstance().setValue(PreferenceKey.bSaveDataOnChange, "false");
						break;
					case "false":
						Preferences.getInstance().setValue(PreferenceKey.bSaveDataOnChange, "true");
						break;
				}
				if (Preferences.getInstance().getValue(PreferenceKey.bSaveDataOnChange).equals("true"))
					Preferences.getInstance().save();
				break;
			case "3":
				Preferences.getInstance().save();
				break;
		}
	}
}

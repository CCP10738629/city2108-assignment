package entrysystem.ui;

/**
 * A class for presenting console menu choices for menu consistency.
 */
final class ConsoleMenuChoice {

	/**
	 * The key.
	 */
	final String key;
	/**
	 * The description.
	 */
	final String desc;

	/**
	 * Instantiates a new Console menu choice.
	 *
	 * @param key  the key
	 * @param desc the desc
	 */
	ConsoleMenuChoice(String key, String desc) {
		this.key = key;
		this.desc = desc;
	}

	/**
	 * Print the choice.
	 */
	void print() {
		System.out.println((new String(new char[15-key.length()]).replace("\0", " ")) + key + "    " + desc);
	}
}

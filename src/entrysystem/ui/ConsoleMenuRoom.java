package entrysystem.ui;

import entrysystem.data.RoomFile;

/**
 * Menu for rooms.
 */
final class ConsoleMenuRoom extends ConsoleMenuData {

	@Override
	protected void printData() {
		RoomFile.getInstance().print();
	}

	@Override
	protected String getTitle() {
		return "Rooms";
	}

	@Override
	protected ConsoleMenuChoice[] getChoiceArray() {
		return new ConsoleMenuChoice[] {
				new ConsoleMenuChoice("1", "Add a room"),
				new ConsoleMenuChoice("2", "Edit or delete a room"),
				new ConsoleMenuChoice("3", "Save room data"),
				new ConsoleMenuChoice("4", "Reload rooms file")
		};
	}

	@Override
	protected ConsoleMenuChoice getExitChoice() {
		return new ConsoleMenuChoice("0 or \"back\"", "go back to the main menu");
	}

	@Override
	protected void process(String input) {
		switch (input) {
			case "1":
				RoomFile.getInstance().addViaConsole();
				break;
			case "2":
				Console.setActiveMenu(new ConsoleMenuRoomEdit());
				break;
			case "3":
				RoomFile.getInstance().save();
				break;
			case "4":
				RoomFile.getInstance().load();
				break;
			case "0":
			case "back":
				Console.setActiveMenu(new ConsoleMenuMain());
				break;
		}
	}
}

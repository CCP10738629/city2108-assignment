package entrysystem.ui;

import java.util.Scanner;

/**
 * Abstract class for console menus.
 */
abstract class ConsoleMenu {

	private static final Scanner INPUT = new Scanner(System.in);

	/**
	 * Gets title of the menu.
	 *
	 * @return the title
	 */
	protected abstract String getTitle();

	/**
	 * Gets the menu's array of ConsoleMenuChoice.
	 *
	 * @return the ConsoleMenuChoice[]
	 */
	protected abstract ConsoleMenuChoice[] getChoiceArray();

	/**
	 * Gets ConsoleMenuChoice for cancel/exit
	 *
	 * @return the exit ConsoleMenuChoice
	 */
	protected abstract ConsoleMenuChoice getExitChoice();

	/**
	 * Run the menu.
	 */
	void run() {
		print();
		process(getInput().toLowerCase());
	}

	/**
	 * Gets input from the user via console.
	 *
	 * @return the user input
	 */
	protected String getInput() {
		return INPUT.nextLine().trim();
	}

	/**
	 * Clear console and print the menu.
	 */
	protected void print() {
		printClear();
		printMenu();
	}

	/**
	 * Clear the console.
	 */
	protected void printClear() {
		System.out.println(new String(new char[40]).replace("\0", "\n"));
	}

	/**
	 * Print the menu.
	 */
	protected void printMenu() {
		System.out.println(getTitle() + "\n\nType to choose the corresponding option\n");
		for (ConsoleMenuChoice choice : getChoiceArray())
			choice.print();
		System.out.println();
		getExitChoice().print();
		System.out.println();
		if (!Console.messageBuffer.printToConsole())
			System.out.println();
		System.out.print("  >\t");
	}

	/**
	 * Method for processing input on the current menu.
	 *
	 * @param input the user input
	 */
	protected abstract void process(String input);
}

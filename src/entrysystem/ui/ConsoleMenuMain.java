package entrysystem.ui;

import entrysystem.data.*;
import entrysystem.log.Log;

/**
 * Main menu
 */
final class ConsoleMenuMain extends ConsoleMenu {

	@Override
	protected String getTitle() {
		return "Main Menu";
	}

	@Override
	protected ConsoleMenuChoice[] getChoiceArray() {
		return new ConsoleMenuChoice[] {
				new ConsoleMenuChoice("1", "view and manage rooms"),
				new ConsoleMenuChoice("2", "view and manage ID cards"),
				new ConsoleMenuChoice("3", "simulate a card swipe"),
				new ConsoleMenuChoice("4", "put a room in or out of emergency state"),
				new ConsoleMenuChoice("5", "view swipe log"),
				new ConsoleMenuChoice("6", "manage preferences")
		};
	}

	@Override
	protected ConsoleMenuChoice getExitChoice() {
		return new ConsoleMenuChoice("0 or \"exit\"", "exit the program");
	}

	@Override
	protected void process(String input) {
		switch (input) {
			case "0":
			case "exit":
				Console.setActiveMenu(null);
				UI.setActiveUI(null);
				break;
			case "1":
				Console.setActiveMenu(new ConsoleMenuRoom());
				break;
			case "2":
				Console.setActiveMenu(new ConsoleMenuIdCard());
				break;
			case "3":
				swipe();
				break;
			case "4":
				activateEmergency();
				break;
			case "5":
				Console.setActiveMenu(new ConsoleMenuLog());
				break;
			case "6":
				Console.setActiveMenu(new ConsoleMenuPreference());
				break;
		}
	}

	private void swipe() { //TODO refactor

		boolean chosen = false;
		int card = -1, room = -1;
		long msSinceMidnight = System.currentTimeMillis() % 86400000;

		String input;

		IdCardFile.getInstance().print();
		System.out.print("Type 'cancel' to return to the previous menu.\n\n");

		while (!chosen) {

			System.out.print("Enter a card ID: ");
			input = getInput();

			if (input.equalsIgnoreCase("cancel"))
				return;

			try {
				card = Integer.parseInt(input);
			} catch (NumberFormatException e) {
				System.out.println("Invalid input.\n");
				continue;
			}

			if (IdCardFile.getInstance().get(card) == null) {
				System.out.println("Not a valid ID.\n");
				continue;
			}
			chosen = true;
		}

		chosen = false;
		RoomFile.getInstance().print();
		System.out.print("Type 'cancel' to return to the previous menu.\n\n");

		while (!chosen) {

			System.out.print("Enter a room ID: ");
			input = getInput();

			if (input.equalsIgnoreCase("cancel"))
				return;

			try {
				room = Integer.parseInt(input);
			} catch (NumberFormatException e) {
				System.out.println("Invalid input.\n");
				continue;
			}

			if (RoomFile.getInstance().get(room) == null) {
				System.out.println("Not a valid ID.\n");
				continue;
			}
			chosen = true;
		}

		Room r = (Room) RoomFile.getInstance().get(room);
		IdCard c = (IdCard) IdCardFile.getInstance().get(card);
		boolean	allow = false;

		switch (c.getType()) {
			case ID_CARD_STAFF:
				switch (r.getType()) {
					case ROOM_STAFF:
					case ROOM_LECTURE:
					case ROOM_TEACHING:
					case ROOM_SECURE:
						if (!r.getEmergencyActive() && msSinceMidnight > 19800000) // 05:30
							allow = true;
				}
				break;
			case ID_CARD_STUDENT:
				switch (r.getType()) {
					case ROOM_LECTURE:
					case ROOM_TEACHING:
						if (!r.getEmergencyActive() && msSinceMidnight > 30600000 && msSinceMidnight < 79200000) // 08:30 - 22:00
							allow = true;
				}
				break;
			case ID_CARD_VISITOR:
				if (r.getType() == RoomType.ROOM_LECTURE && !r.getEmergencyActive() && msSinceMidnight > 30600000 && msSinceMidnight < 79200000) // 08:30 - 22:00
					allow = true;
				break;
			case ID_CARD_CLEANER:
				switch (r.getType()) {
					case ROOM_STAFF:
					case ROOM_LECTURE:
					case ROOM_TEACHING:
						if (!r.getEmergencyActive() && ((msSinceMidnight > 19800000 && msSinceMidnight < 37800000) || (msSinceMidnight > 63000000 && msSinceMidnight < 81000000))) // 05:30 - 10:30 or 17:30 - 22:30
							allow = true;
				}
				break;
			case ID_CARD_MANAGER:
				if (!r.getEmergencyActive())
					allow = true;
				break;
			case ID_CARD_SECURITY:
				allow = true;
				break;
			case ID_CARD_EMERGENCY:
				if (r.getEmergencyActive())
					allow = true;
				break;
		}
		Log.swipeLog.append((allow ? "GRANTED" : "DENIED") + "," + c.getId() + "," + c.getName() + "," + r.getId() + "," + r.getName() + "," + r.getEmergencyActive());
	}

	private void activateEmergency() { //TODO refactor
		boolean chosen = false;
		int room;
		RoomFile.getInstance().print();
		System.out.print("Type 'cancel' to return to the previous menu.\n\n");

		while (true) {

			System.out.print("Enter a room ID: ");
			String input = getInput();

			if (input.equalsIgnoreCase("cancel"))
				return;

			try {
				room = Integer.parseInt(input);
			} catch (NumberFormatException e) {
				System.out.println("Invalid input.\n");
				continue;
			}

			if (RoomFile.getInstance().get(room) == null) {
				System.out.println("Not a valid ID.\n");
				continue;
			}
			Room r = (Room) RoomFile.getInstance().get(room);
			r.toggleEmergency();
			Log.programLog.append("Room state toggled for " + r.getName());
			return;
		}
	}
}
package entrysystem.ui;

import entrysystem.log.Log;

/**
 * The type Console.
 */
public final class Console extends UI {

	private static ConsoleMenu activeMenu = new ConsoleMenuMain();
	/**
	 * The constant messageBuffer.
	 */
	public static final MessageBuffer messageBuffer = new MessageBuffer();

	/**
	 * Instantiates a new Console.
	 */
	protected Console() {
		Log.programLog.append("Using console mode.");
	}

	@Override
	protected void run() {
		if (activeMenu != null)
			activeMenu.run();
	}

	/**
	 * Sets active menu.
	 *
	 * @param menu the menu
	 */
	protected static void setActiveMenu(ConsoleMenu menu) {
		activeMenu = menu;
	}
}
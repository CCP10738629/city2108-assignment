package entrysystem.ui;

/**
 * Console menu for editing data.
 */
abstract class ConsoleMenuDataEdit extends ConsoleMenuData {

	protected void print() {
		if (selectData())
			super.print();
	}

	/**
	 * Method for selecting data for editing.
	 *
	 * @return returns true upon complete, false if cancelled.
	 */
	protected abstract boolean selectData();
}

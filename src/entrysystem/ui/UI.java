package entrysystem.ui;

import entrysystem.pref.Preferences;
import entrysystem.pref.PreferenceKey;

/**
 * The type UI.
 */
public abstract class UI {

	private static UI activeUI = null;

	/**
	 * Sets the active UI (console or GUI)
	 *
	 * @param ui the ui
	 */
	protected static void setActiveUI(UI ui) {
		activeUI = ui;
	}

	/**
	 * Initialise UI.
	 */
	public static void init() {
		if (Preferences.getInstance().getValue(PreferenceKey.bGui).equals("true"))
			activeUI = new Gui();
		else
			activeUI = new Console();
		while (activeUI != null)
			activeUI.run();
	}

	/**
	 * Method for running the UI.
	 */
	protected abstract void run();
}
package entrysystem.ui;

import entrysystem.data.IdCard;
import entrysystem.data.IdCardFile;
import entrysystem.data.IdCardType;
import entrysystem.pref.PreferenceKey;
import entrysystem.pref.Preferences;

/**
 * Menu for editing an ID card.
 */
final class ConsoleMenuIdCardEdit extends ConsoleMenuDataEdit {

	private IdCard chosen;

	@Override
	protected void run() {
		print();
		if (chosen != null)
			process(getInput().toLowerCase());
	}

	@Override
	protected void printData() {
		chosen.printForMenu();
	}

	@Override
	protected String getTitle() {
		return "Edit ID Card";
	}

	@Override
	protected ConsoleMenuChoice[] getChoiceArray() {
		return new ConsoleMenuChoice[] {
				new ConsoleMenuChoice("1", "change the name"),
				new ConsoleMenuChoice("2", "change the card type"),
				new ConsoleMenuChoice("3", "delete the card")
		};
	}

	@Override
	protected ConsoleMenuChoice getExitChoice() {
		return new ConsoleMenuChoice("0 or \"back\"", "go back to the user menu");
	}

	@Override
	protected void process(String input) { //TODO refactor
		switch (input) {
			case "1":
				System.out.print("Enter a name or type 'cancel': ");
				String name = getInput();
				if (name.equalsIgnoreCase("cancel"))
					break;
				chosen.setName(name);
				if (Preferences.getInstance().getValue(PreferenceKey.bSaveDataOnChange).equals("true"))
					IdCardFile.getInstance().save();
				break;
			case "3":
				System.out.println("Confirm ID card deletion (Y/N)");
				if (getInput().equalsIgnoreCase("y"))
					IdCardFile.getInstance().remove(chosen.getId());
				else
					Console.messageBuffer.append("Deletion cancelled.");
				break;
			case "2":
				IdCardType.print();
				boolean typeSet = false;
				while (!typeSet) {
					System.out.print("Enter an integer to choose a card type, or type 'cancel': ");
					String type = getInput();
					switch (type) {
						default:
							if (type.equalsIgnoreCase("cancel")) {
								typeSet = true;
								break;
							}
							System.out.println("Invalid input.\n");
							break;
						case "0":
						case "1":
						case "2":
						case "3":
						case "4":
						case "5":
						case "6":
						case "7":
							chosen.setType(IdCardType.fromIntString(type));
							typeSet = true;
							if (Boolean.parseBoolean(Preferences.getInstance().getValue(PreferenceKey.bSaveDataOnChange)))
								IdCardFile.getInstance().save();
							break;
					}
				}
				break;
			case "0":
			case "back":
				Console.setActiveMenu(new ConsoleMenuIdCard());
				break;
		}
	}

	@Override
	protected boolean selectData() {// TODO refactor

		if (chosen != null)
			return true;

		System.out.print("Type 'cancel' to return to the previous menu.\n\n");

		while (true) {

			System.out.print("Enter an ID: ");
			String input = getInput();

			if (input.equalsIgnoreCase("cancel")) {
				Console.setActiveMenu(new ConsoleMenuIdCard());
				return false;
			}

			try {
				if ((chosen = (IdCard) IdCardFile.getInstance().get(Integer.parseInt(input))) == null) {
					System.out.println("Not a valid ID.\n");
				} else
					return true;
			} catch (NumberFormatException e) {
				System.out.println("Invalid input.\n");
			}
		}
	}
}
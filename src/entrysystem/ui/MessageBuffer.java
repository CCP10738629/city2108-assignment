package entrysystem.ui;

/**
 * A message buffer used for the Console
 */
public class MessageBuffer {

	private String buffer = "";

	/**
	 * Prints contents of the buffer to console then clears it.
	 *
	 * @return Returns true upon success or false if the buffer is empty.
	 */
	public boolean printToConsole() {
		if (buffer.length() > 0) {
			System.out.println(buffer);
			buffer = "";
			return true;
		}
		return false;
	}

	/**
	 * Append a message to the buffer.
	 *
	 * @param s the message
	 */
	public void append(String s) {
		buffer += s.trim() + "\n";
	}
}

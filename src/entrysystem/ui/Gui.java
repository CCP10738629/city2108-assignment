package entrysystem.ui;

import entrysystem.log.Log;
import entrysystem.pref.PreferenceKey;
import entrysystem.pref.Preferences;

/**
 * The type Gui. (unimplemented)
 */
final class Gui extends UI {

	/**
	 * Instantiates a new Gui.
	 */
	protected Gui() {
		Log.programLog.append("Using GUI mode.");
	}

	@Override
	public void run() {
		Log.programLog.append("GUI mode is not implemented yet.");
		UI.setActiveUI(new Console());
		Preferences.getInstance().setValue(PreferenceKey.bGui, "false");
	}
}

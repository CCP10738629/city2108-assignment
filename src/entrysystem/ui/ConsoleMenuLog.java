package entrysystem.ui;

import entrysystem.log.Log;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 * Menu for reviewing daily swipe log files.
 */
final class ConsoleMenuLog extends ConsoleMenuData {

	private int activeFile;
	private final ArrayList<File> filesInFolder = new ArrayList<>();

	ConsoleMenuLog() {
		String[] files = new File(Log.swipeLog.getLogDir()).list();
		if (files != null) {
			for (String s : files)
				if (
						s.length() == Log.swipeLog.getLogName().length() + 15
					&&	s.contains(Log.swipeLog.getLogName())
					&&	s.contains(".txt")
				)
					filesInFolder.add(new File(Log.swipeLog.getLogDir() + s));
			Collections.sort(filesInFolder);
			activeFile = filesInFolder.size() - 1;
		}
	}

	private void openNewerFile() {
		if (activeFile < filesInFolder.size() - 1)
			activeFile++;
		else
			Log.programLog.append("Already displaying newest file.");
	}

	private void openOlderFile() {
		if (activeFile > 0)
			activeFile--;
		else
			Log.programLog.append("Already displaying oldest file.");
	}

	@Override
	protected String getTitle() {
		return "Swipe Log Menu";
	}

	@Override
	protected ConsoleMenuChoice[] getChoiceArray() {
		return new ConsoleMenuChoice[] {
				new ConsoleMenuChoice("1", "load previous log"),
				new ConsoleMenuChoice("2", "load next log")
		};
	}

	@Override
	protected ConsoleMenuChoice getExitChoice() {
		return new ConsoleMenuChoice("0 or back", "go back to the main menu");
	}

	@Override
	protected void process(String input) {
		switch (input) {
			case "0":
			case "back":
				Console.setActiveMenu(new ConsoleMenuMain());
				break;
			case "1":
				openOlderFile();
				break;
			case "2":
				openNewerFile();
				break;
		}
	}

	@Override
	protected void printData() {
		if (filesInFolder.isEmpty())
			System.out.println("No swipe logs available.\n");
		else
			try {
				Scanner s = new Scanner(filesInFolder.get(activeFile));
				if (s.hasNextLine())
					s.nextLine();
				if (!s.hasNextLine())
					System.out.println("\tLog empty\n");
				while (s.hasNextLine()) {
					String[] swipe = s.nextLine().split(",");
					if (swipe.length != 7)
						continue;
					System.out.println(
							"\tTime: " + swipe[0]
							+ "\n\tUser: " + swipe[3] + " (ID: " + swipe[2] + ")"
							+ "\n\tRoom: " + swipe[5] + " (ID: " + swipe[4] + ")"
							+ "\n\tEmergency: " + swipe[6]
							+ "\n\tAccess: " + swipe[1]
							+ "\n"
					);
				}
				System.out.println("Displaying: " + filesInFolder.get(activeFile).getName() + "\n");
			} catch (IOException i){
				System.out.println("Error reading: " + filesInFolder.get(activeFile).getName() + "\n");
			}
	}
}
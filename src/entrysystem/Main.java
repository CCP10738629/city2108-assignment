package entrysystem;

import entrysystem.log.Log;
import entrysystem.ui.UI;

public class Main {
	/**
	 * The entry point of application.
	 *
	 * @param args the input arguments
	 */
	public static void main(String[] args) {

		System.out.println();

		Log.programLog.append("Initialising program.");
		Log.programLog.append("Log directory: " + Log.getLogFolder());

		UI.init();
	}
}
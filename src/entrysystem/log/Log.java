package entrysystem.log;

import entrysystem.ui.Console;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

/**
 * Responsible for managing log files.
 */
public abstract class Log {
	/**
	 * The program log object.
	 */
	public static final Log	programLog = new RuntimeLog("ProgramLog"),
	/**
	 * The swipe log object.
	 */
	swipeLog = new DailyLog("SwipeLog");
	private static final String LOGS_FOLDER = "/logs/";
	private static final SimpleDateFormat	DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd"),
											TIME_FORMATTER = new SimpleDateFormat("HH:mm:ss"),
											DATETIME_FORMATTER = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");

	/**
	 * Instantiates a new Log.
	 *
	 * @param logName the log name
	 */
	protected Log(String logName) {
		this.logName = logName;
	}

	/**
	 * The Log name, as it will appear in the file system.
	 * This will be "Log" by default.
	 */
	protected String logName = "Log";

	/**
	 * Returns the absolute path to the folder where logs are being stored.
	 *
	 * @return the log folder
	 */
	public static String getLogFolder() {
		return new File(LOGS_FOLDER).getAbsolutePath();
	}

	/**
	 * Gets date.
	 *
	 * @param t A Timestamp object.
	 * @return A date formatted as YYYY-MM-DD
	 */
	protected static String getDate(Timestamp t) {
		return DATE_FORMATTER.format(t);
	}

	/**
	 * Gets time.
	 *
	 * @param t A Timestamp object.
	 * @return A time formatted as HH:MM:SS
	 */
	protected static String getTime(Timestamp t) {
		return TIME_FORMATTER.format(t);
	}

	/**
	 * Gets date time.
	 *
	 * @param t A Timestamp object.
	 * @return A date/time formatted as YYYY-MM-DD-HH-MM-SS (filename safe)
	 */
	protected static String getDateTime(Timestamp t) {
		return DATETIME_FORMATTER.format(t);
	}

	/**
	 * Gets the relative directory for this Log object as a string.
	 *
	 * @return the log dir
	 */
	public String getLogDir() {
		return LOGS_FOLDER + logName + "/";
	}

	/**
	 * Gets log name.
	 *
	 * @return the log name
	 */
	public String getLogName() {
		return logName;
	}

	/**
	 * Appends a message to the associated log file for this Log object.
	 * Timestamp is automatically prepended.
	 * <br><br>
	 * Will also print to console by default, use append(String message, <b>false</b>) to avoid this.
	 *
	 * @param message the message
	 */
	public void append(String message) {
		append(message, true);
	}

	/**
	 * Appends a message to the associated log file for this Log object.
	 * Timestamp is automatically prepended.
	 * Optionally also prints to console if <u>print</u> is set to <b>true</b>.
	 *
	 * @param message the message
	 * @param print   the print
	 */
	public void append(String message, boolean print) {
		Timestamp t = new Timestamp(System.currentTimeMillis());
		String s = formatMessage(t, message);
		if (print)
			Console.messageBuffer.append(getDate(t) + " " + getTime(t) + " " + logName + ": " + message);
		try {
			appendStrategy(t, s);
		} catch (IOException ignored) {
			System.out.println("Failed to write to log: " + logName + ".");
		}
	}

	/**
	 * Defines a strategy for appending a log message
	 *
	 * @param t the Timestamp generated when append() was called.
	 * @param s the formatted log message to append.
	 * @throws IOException the io exception
	 */
	protected abstract void appendStrategy(Timestamp t, String s) throws IOException;

	/**
	 * Defines a strategy for formatting a log message
	 *
	 * @param t the Timestamp generated when append() was called.
	 * @param s the log message to format.
	 * @return the formatted log message.
	 */
	protected abstract String formatMessage(Timestamp t, String s);
}
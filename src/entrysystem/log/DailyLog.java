package entrysystem.log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;

/**
 * A Log strategy subclass. Creates one log file per day. Appends to today's file if it already exists.
 */
class DailyLog extends Log {

	/**
	 * Instantiates a new Daily log.
	 *
	 * @param logName the log name
	 */
	protected DailyLog(String logName) {
		super(logName);
	}

	@Override
	protected void appendStrategy(Timestamp t, String s) throws IOException {
		String dir = getLogDir();
		Files.createDirectories(Paths.get(dir));
		BufferedWriter w = new BufferedWriter(new FileWriter(dir + logName + "-" + getDate(t) + ".txt", true));
		if (new File(dir + logName + "-" + getDate(t) + ".txt").length() == 0)
			w.write("time,access,cardID,name,roomID,room,emergency\n");
		w.write(s);
		w.close();
	}

	@Override
	protected String formatMessage(Timestamp t, String s) {
		return getTime(t) + "," + s + "\n";
	}
}
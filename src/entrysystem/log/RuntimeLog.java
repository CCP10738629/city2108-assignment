package entrysystem.log;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;

/**
 * A Log strategy subclass. Creates a new log file every time the program is initialised.
 */
class RuntimeLog extends Log {

	private BufferedWriter w = null;

	/**
	 * Instantiates a new Runtime log.
	 *
	 * @param logName the log name
	 */
	protected RuntimeLog(String logName) {
		super(logName);
	}

	@Override
	protected void appendStrategy(Timestamp t, String s) throws IOException {
		if (this.w == null) {
			String dir = getLogDir();
			Files.createDirectories(Paths.get(dir));
			this.w = new BufferedWriter(new FileWriter(dir + logName + "-" + getDateTime(t) + ".txt", true));
		}
		w.write(s);
		w.flush();
	}

	@Override
	protected String formatMessage(Timestamp t, String s) {
		return getDate(t) + " " + getTime(t) + " - " + s + "\n";
	}
}

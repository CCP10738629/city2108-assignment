package entrysystem.data;

/**
 * The type Data.
 */
abstract class Data {

	private static int maxId = -1;

	/**
	 * The Id.
	 */
	protected final int id;

	/**
	 * Instantiates a new Data, incrementing maxId.
	 */
	protected Data() {
		id = ++maxId;
	}

	/**
	 * Gets id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Print data to console when listing all data.
	 */
	abstract void printForTable();

	/**
	 * Print data to console when in the edit menu.
	 */
	public abstract void printForMenu();
}

package entrysystem.data;

/**
 * The enum Room type.
 */
public enum RoomType {
	ROOM_LECTURE,
	ROOM_TEACHING,
	ROOM_STAFF,
	ROOM_SECURE,
	ROOM_VOID;

	/**
	 * Returns a room type from an integer.
	 *
	 * @param i an integer
	 * @return the room type
	 */
	public static RoomType fromInt(int i) {
		switch (i) {
			case 0: return RoomType.ROOM_LECTURE;
			case 1: return RoomType.ROOM_TEACHING;
			case 2: return RoomType.ROOM_STAFF;
			case 3: return RoomType.ROOM_SECURE;
			default: return RoomType.ROOM_VOID;
		}
	}

	/**
	 * Returns a RoomType from an integer as a String.
	 *
	 * @param s an integer as a string
	 * @return the room type
	 */
	public static RoomType fromIntString(String s) {
		switch (s) {
			case "0": return RoomType.ROOM_LECTURE;
			case "1": return RoomType.ROOM_TEACHING;
			case "2": return RoomType.ROOM_STAFF;
			case "3": return RoomType.ROOM_SECURE;
			default: return RoomType.ROOM_VOID;
		}
	}

	/**
	 * Returns an integer from a RoomType
	 *
	 * @param i a RoomType
	 * @return the int
	 */
	public static int toInt(RoomType i) {
		switch (i) {
			case ROOM_LECTURE: return 0;
			case ROOM_TEACHING: return 1;
			case ROOM_STAFF: return 2;
			case ROOM_SECURE: return 3;
			default: return -1;
		}
	}

	/**
	 * Returns the name of the RoomType
	 *
	 * @param i a RoomType
	 * @return the RoomType name
	 */
	public static String toString(RoomType i) {
		return toString(toInt(i));
	}

	/**
	 * Returns the name of the RoomType from an integer.
	 *
	 * @param i an integer
	 * @return the RoomType name
	 */
	public static String toString(int i) {
		switch (i) {
			case 0: return "Lecture Hall";
			case 1: return "Teaching Room";
			case 2: return "Staff Room";
			case 3: return "Secure Room";
			default: return "Invalid/Deactivated";
		}
	}

	/**
	 * Print all of the room type options to the console.
	 */
	public static void print() {
		for (int i = 0; i < RoomType.values().length; i++)
			System.out.println(i + ": " + RoomType.toString(i));
	}
}

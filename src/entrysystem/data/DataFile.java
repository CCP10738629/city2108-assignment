package entrysystem.data;

import entrysystem.log.Log;

import java.io.*;
import java.util.ArrayList;
import java.util.Stack;

/**
 * The type DataFile, handles a collection of Data.
 */
abstract class DataFile {

	/**
	 * The ArrayList of Data.
	 */
	protected final ArrayList<Data> ARRAY = new ArrayList<>(0);
	/**
	 * The File that stores the data.
	 */
	protected final File FILE = new File(getFileName());
	/**
	 * The maxId for ensuring uniqueness.
	 */
	protected int maxId = -1;

	/**
	 * Instantiates a new DataFile, loading data from file.
	 */
	protected DataFile() {
		load();
	}

	/**
	 * Saves held data to the file.
	 *
	 * @return returns true if successful, false if not.
	 */
	public boolean save() {
		try {
			// create an ID card file if needed
			if (FILE.createNewFile())
				Log.programLog.append("Created file: " + FILE);

			Log.programLog.append("Saving: " + FILE.getName());

			// Clear file
			BufferedWriter writer = new BufferedWriter(new FileWriter(FILE.getPath(), false));
			writer.write("");
			writer.close();

			// Append each card
			writer = new BufferedWriter(new FileWriter(FILE.getPath(), true));
			for (Data data : ARRAY)
				appendStrategy(writer, data);
			writer.close();

			Log.programLog.append("Done saving: " + FILE.getName());
		}
		catch (IOException | SecurityException e) {
			Log.programLog.append("Error saving: " + FILE.getName());
			return false;
		}
		return true;
	}

	/**
	 * Loads data from file.
	 *
	 * @return returns true if successful, false if not.
	 */
	public boolean load() {
		try {
			if (FILE.createNewFile())
				Log.programLog.append("Created file: " + FILE);
			else {

				BufferedReader reader = new BufferedReader(new FileReader(FILE));
				Stack<Integer> ignored = new Stack<>();

				Log.programLog.append("Loading: " + FILE.getName());
				ARRAY.clear();
				int lineNumber = 1;
				for(String line; (line = reader.readLine()) != null; lineNumber++)
					if (!loadLineStrategy(line))
						ignored.push(lineNumber);
				reader.close();
				Log.programLog.append("Done loading: " + FILE.getName());

				if (ignored.size() > 0) {
					StringBuilder ignoredString = new StringBuilder("Some lines are invalid and were ignored: ");
					boolean first = true;
					for (int n : ignored) {
						ignoredString.append(n);
						if (!first)
							ignoredString.append(",");
						first = false;
					}
					Log.programLog.append(ignoredString.toString());
				}
			}
		} catch (IOException | SecurityException e) {
			Log.programLog.append("Error loading: " + FILE.getName());
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Get a Data from the collection by searching for its unique ID.
	 *
	 * @param id the desired Data's ID
	 * @return the data
	 */
	public Data get(int id) {
		for (Data i : ARRAY)
			if (i.id == id)
				return i;
		return null;
	}

	/**
	 * Gets the size of the collection.
	 *
	 * @return the size
	 */
	public int getSize() {
		return ARRAY.size();
	}

	/**
	 * Removes and deletes Data from the collection.
	 *
	 * @param id the id
	 */
	public void remove(int id) {
		for (Data i : ARRAY)
			if (i.getId() == id) {
				ARRAY.remove(i);
				return;
			}
	}

	/**
	 * Prints the collection to console in table form.
	 */
	public void print() {
		if (ARRAY.isEmpty())
			printError();
		else {
			printHead();
			for (Data i : ARRAY)
				i.printForTable();
		}
		System.out.println();
	}

	/**
	 * The message to display if there is no data.
	 */
	protected abstract void printError();

	/**
	 * Method for printing the heading for the table.
	 */
	protected abstract void printHead();

	/**
	 * Gets file name.
	 *
	 * @return the file name
	 */
	protected abstract String getFileName();

	/**
	 * The strategy for appending a piece of data to the file.
	 *
	 * @param writer the writer
	 * @param data   the data
	 * @throws IOException the io exception
	 */
	protected abstract void appendStrategy(BufferedWriter writer, Data data) throws IOException;

	/**
	 * The strategy for loading each piece of data from the file.
	 *
	 * @param csvLine the csv line
	 * @return Returns true if the line represents valid data, false if not.
	 */
	protected abstract boolean loadLineStrategy(String csvLine);

	/**
	 * Method for adding a piece of data via console interface.
	 */
	public abstract void addViaConsole();
}

package entrysystem.data;

/**
 * The type ID card.
 */
public class IdCard extends Data {

	private String name;
	private IdCardType type;

	/**
	 * Instantiates a new ID card, converting an integer to a card type.
	 *
	 * @param name the name
	 * @param type the type
	 */
	IdCard(String name, int type) {
		this(name, IdCardType.fromInt(type));
	}

	/**
	 * Instantiates a new ID card.
	 *
	 * @param name the name
	 * @param type the type
	 */
	IdCard(String name, IdCardType type) {
		super();
		this.name = name;
		this.type = type;
	}

	/**
	 * Gets name associated with the card.
	 *
	 * @return the name
	 */
	public String getName() { return name; }

	/**
	 * Gets card type.
	 *
	 * @return the type
	 */
	public IdCardType getType() { return type; }

	/**
	 * Sets name associated with the card.
	 *
	 * @param name the name
	 */
	public void setName(String name) {
		if (!name.trim().equals(""))
			this.name = name;
	}

	/**
	 * Sets card type.
	 *
	 * @param type the type
	 */
	public void setType(IdCardType type) {
		this.type = type;
	}

	/**
	 * Sets card type, converting an integer to IdCardType.
	 *
	 * @param type the type
	 */
	public void setType(int type) {
		if (type >= 0 && type < IdCardType.values().length)
			this.type = IdCardType.fromInt(type);
	}

	@Override
	void printForTable() {
		String typeString = IdCardType.toString(type);
		System.out.println(
				id + new String(new char[5-Integer.toString(id).length()]).replace("\0", " ") + "| " +
				name + new String(new char[29-name.length()]).replace("\0", " ") + "| " +
				typeString
		);
	}

	@Override
	public void printForMenu() {
		System.out.println(
				"ID: " + id + "\n" +
				"Name: " + name + "\n" +
				"Card type: " + IdCardType.toString(type) + "\n"
		);
	}
}
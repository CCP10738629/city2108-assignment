package entrysystem.data;

/**
 * The type Room.
 */
public class Room extends Data {

	private String name;
	private RoomType type;
	private boolean emergency = false;

	/**
	 * Instantiates a new Room, converting an integer to a RoomType.
	 *
	 * @param name the name
	 * @param type the type
	 */
	Room(String name, int type) {
		this(name, RoomType.fromInt(type));
	}

	/**
	 * Instantiates a new Room.
	 *
	 * @param name the name
	 * @param type the type
	 */
	Room(String name, RoomType type) {
		super();
		this.name = name;
		this.type = type;
	}

	/**
	 * Gets room name.
	 *
	 * @return the name
	 */
	public String getName() { return name; }

	/**
	 * Gets room type.
	 *
	 * @return the type
	 */
	public RoomType getType() { return type; }

	/**
	 * Gets emergency active.
	 *
	 * @return true if emergency is active, false if not.
	 */
	public boolean getEmergencyActive() { return emergency; }

	/**
	 * Sets room name.
	 *
	 * @param name the name
	 */
	public void setName(String name) {
		if (!name.trim().equals(""))
			this.name = name;
	}

	/**
	 * Sets room type.
	 *
	 * @param type the type
	 */
	public void setType(RoomType type) {
		this.type = type;
	}

	/**
	 * Sets room type, converting an integer to RoomType.
	 *
	 * @param type the type
	 */
	public void setType(int type) {
		if (type >= 0 && type < IdCardType.values().length)
			this.type = RoomType.fromInt(type);
	}

	/**
	 * Toggle emergency.
	 */
	public void toggleEmergency() {
		emergency = !emergency;
	}

	@Override
	void printForTable() {
		String typeString = RoomType.toString(type);
		System.out.println(
				id + new String(new char[5-Integer.toString(id).length()]).replace("\0", " ") + "| " +
				name + new String(new char[29-name.length()]).replace("\0", " ") + "| " +
				typeString + new String(new char[19-typeString.length()]).replace("\0", " ") + "| " +
				(emergency ? "!!!" : "")
		);
	}

	@Override
	public void printForMenu() {
		System.out.println(
				"ID: " + id + "\n" +
				"Name: " + name + "\n" +
				"Room type: " + RoomType.toString(type) + "\n"
		);
	}
}
package entrysystem.data;

import entrysystem.pref.PreferenceKey;
import entrysystem.pref.Preferences;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 * The type ID card file.
 */
public final class IdCardFile extends DataFile { // final to prevent child classes

	private static final IdCardFile instance = new IdCardFile();

	/**
	 * Gets the singleton instance.
	 *
	 * @return the singleton instance.
	 */
	public static IdCardFile getInstance() {
		return instance;
	}

	@Override
	protected String getFileName() {
		return "id_card_list.txt";
	}

	@Override
	protected void appendStrategy(BufferedWriter writer, Data data) throws IOException {
		IdCard card = ((IdCard) data);
		writer.append(String.valueOf(card.getId())).append(",").append(card.getName()).append(",").append(String.valueOf(IdCardType.toInt(card.getType()))).append("\n");
	}

	@Override
	protected boolean loadLineStrategy(String csvLine) {
		String[] data = csvLine.split(",");	// Split each line into an array (CSV)
		if (data.length == 3) {						// Expecting 3 values per line
			int id, type;
			try {									// id and type must be integers
				id = Integer.parseInt(data[0]);
				type = Integer.parseInt(data[2]);
			} catch (NumberFormatException e) { return false; }
			if (id >= 0 && type >= 0 && type < IdCardType.values().length) {    // Check these integers are in the expected range
				ARRAY.add(new IdCard(data[1], type));
				if (id > maxId)
					maxId = id;
			}
			else return false;
		} else return false;
		return true;
	}

	public void addViaConsole() {

		Scanner input = new Scanner(System.in);

		System.out.print(
				"Type 'cancel' to return to the previous menu.\n\n" +
				"Enter a name: "
		);
		String name = input.nextLine().trim();
		if (name.equalsIgnoreCase("cancel"))
			return;

		IdCardType.print();

		boolean typeSet = false;
		while (!typeSet) {
			System.out.print("Enter an integer to choose a card type: ");
			String type = input.nextLine().trim();
			switch (type) {
				default:
					if (type.equalsIgnoreCase("cancel"))
						return;
					System.out.println("Invalid input.\n");
					break;
				case "0":
				case "1":
				case "2":
				case "3":
				case "4":
				case "5":
				case "6":
				case "7":
					ARRAY.add(new IdCard(name, IdCardType.fromIntString(type)));
					typeSet = true;
					break;
			}
			if (typeSet && Preferences.getInstance().getValue(PreferenceKey.bSaveDataOnChange).equals("true"))
				IdCardFile.getInstance().save();
		}
	}

	@Override
	protected void printError() {
		System.out.println("No ID cards in the system.");
	}

	@Override
	protected void printHead() {
		System.out.println(
				"ID   | Name                         | Card Type\n" +
				"=====+==============================+=================="
		);
	}
}

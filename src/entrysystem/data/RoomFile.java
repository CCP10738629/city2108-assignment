package entrysystem.data;

import entrysystem.pref.PreferenceKey;
import entrysystem.pref.Preferences;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 * The type Room file.
 */
public final class RoomFile extends DataFile { // final to prevent child classes

	private static final RoomFile instance = new RoomFile();

	/**
	 * Gets singleton instance.
	 *
	 * @return the singleton instance
	 */
	public static RoomFile getInstance() {
		return instance;
	}

	@Override
	protected void printError() {
		System.out.println("No rooms in the system.");
	}

	@Override
	protected void printHead() {
		System.out.println(
				"ID   | Name                         | Card Type          | Emergency\n" +
				"=====+==============================+====================+=========="
		);
	}

	@Override
	public void addViaConsole() {

		Scanner input = new Scanner(System.in);

		System.out.print(
				"Type 'cancel' to return to the previous menu.\n\n" +
				"Enter a name: "
		);
		String name = input.nextLine().trim();
		if (name.equalsIgnoreCase("cancel"))
			return;

		RoomType.print();

		boolean typeSet = false;
		while (!typeSet) {
			System.out.print("Enter an integer to choose a room type: ");
			String type = input.nextLine().trim();
			switch (type) {
				default:
					if (type.equalsIgnoreCase("cancel"))
						return;
					System.out.println("Invalid input.\n");
					break;
				case "0":
				case "1":
				case "2":
				case "3":
				case "4":
					ARRAY.add(new Room(name, RoomType.fromIntString(type)));
					typeSet = true;
					break;
			}
			if (typeSet && Preferences.getInstance().getValue(PreferenceKey.bSaveDataOnChange).equals("true"))
				RoomFile.getInstance().save();
		}
	}

	@Override
	protected String getFileName() {
		return "room_list.txt";
	}

	@Override
	protected void appendStrategy(BufferedWriter writer, Data data) throws IOException {
		Room room = ((Room) data);
		writer.append(String.valueOf(room.getId())).append(",").append(room.getName()).append(",").append(String.valueOf(RoomType.toInt(room.getType()))).append("\n");
	}

	@Override
	protected boolean loadLineStrategy(String csvLine) {
		String[] data = csvLine.split(",");	// Split each line into an array (CSV)
		if (data.length == 3) {						// Expecting 3 values per line
			int id, type;
			try {									// id and type must be integers
				id = Integer.parseInt(data[0]);
				type = Integer.parseInt(data[2]);
			} catch (NumberFormatException e) { return false; }
			if (id >= 0 && type >= 0 && type < RoomType.values().length) {    // Check these integers are in the expected range
				ARRAY.add(new Room(data[1], type));
				if (id > maxId)
					maxId = id;
			}
			else return false;
		} else return false;
		return true;
	}
}

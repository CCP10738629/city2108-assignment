package entrysystem.data;

/**
 * The enum Id card type.
 */
public enum IdCardType {
	ID_CARD_STAFF,
	ID_CARD_STUDENT,
	ID_CARD_VISITOR,
	ID_CARD_CLEANER,
	ID_CARD_MANAGER,
	ID_CARD_SECURITY,
	ID_CARD_EMERGENCY,
	ID_CARD_VOID;

	/**
	 * Returns an IdCardType from an integer.
	 *
	 * @param i an int
	 * @return the id card type
	 */
	public static IdCardType fromInt(int i) {
		switch (i) {
			case 0: return IdCardType.ID_CARD_STAFF;
			case 1: return IdCardType.ID_CARD_STUDENT;
			case 2: return IdCardType.ID_CARD_VISITOR;
			case 3: return IdCardType.ID_CARD_CLEANER;
			case 4: return IdCardType.ID_CARD_MANAGER;
			case 5: return IdCardType.ID_CARD_SECURITY;
			case 6: return IdCardType.ID_CARD_EMERGENCY;
			default: return IdCardType.ID_CARD_VOID;
		}
	}

	/**
	 * Returns an IdCardType from an integer as a string.
	 *
	 * @param s an int as a string
	 * @return the id card type
	 */
	public static IdCardType fromIntString(String s) {
		switch (s) {
			case "0": return IdCardType.ID_CARD_STAFF;
			case "1": return IdCardType.ID_CARD_STUDENT;
			case "2": return IdCardType.ID_CARD_VISITOR;
			case "3": return IdCardType.ID_CARD_CLEANER;
			case "4": return IdCardType.ID_CARD_MANAGER;
			case "5": return IdCardType.ID_CARD_SECURITY;
			case "6": return IdCardType.ID_CARD_EMERGENCY;
			default: return IdCardType.ID_CARD_VOID;
		}
	}

	/**
	 * Returns an integer from an IdCardType
	 *
	 * @param i an IdCardType
	 * @return the corresponding int
	 */
	public static int toInt(IdCardType i) {
		switch (i) {
			case ID_CARD_STAFF: return 0;
			case ID_CARD_STUDENT: return 1;
			case ID_CARD_VISITOR: return 2;
			case ID_CARD_CLEANER: return 3;
			case ID_CARD_MANAGER: return 4;
			case ID_CARD_SECURITY: return 5;
			case ID_CARD_EMERGENCY: return 6;
			default: return -1;
		}
	}

	/**
	 * Returns the name of the IdCardType.
	 *
	 * @param i the
	 * @return the IdCardType name
	 */
	public static String toString(IdCardType i) {
		return toString(toInt(i));
	}

	/**
	 * Returns the name of the IdCardType from an integer.
	 *
	 * @param i the
	 * @return the IdCardType name
	 */
	public static String toString(int i) {
		switch (i) {
			case 0: return "Staff";
			case 1: return "Student";
			case 2: return "Visitor";
			case 3: return "Cleaner";
			case 4: return "Manager";
			case 5: return "Security";
			case 6: return "Emergency Responder";
			default: return "Invalid/Deactivated";
		}
	}

	/**
	 * Print all of the ID card type options to the console.
	 */
	public static void print() {
		for (int i = 0; i < IdCardType.values().length; i++)
			System.out.println(i + ": " + IdCardType.toString(i));
	}
}

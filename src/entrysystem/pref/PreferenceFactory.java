package entrysystem.pref;

/**
 * Factory class to centralise object creation. Uses the PreferenceKey enum to return the matching object.
 */
final class PreferenceFactory { // package-protected
	/**
	 * Gets a preference.
	 *
	 * @param key PreferenceKey enum
	 * @return matching Preference object
	 */
	public Preference getPreference(PreferenceKey key) {
		switch (key) {
			case bGui: return new bGui();
			case bSaveDataOnChange: return new bSaveDataOnChange();
			default: return null;
		}
	}

	/**
	 * Get all preferences.
	 *
	 * @return array of every Preference object
	 */
	public Preference[] getAllPreferences() {
		return new Preference[] {
				new bGui(),
				new bSaveDataOnChange()
		};
	}
}

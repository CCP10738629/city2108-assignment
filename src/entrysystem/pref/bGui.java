package entrysystem.pref;

/**
 * Boolean indicating whether or not GUI mode is active.
 */
final class bGui implements PreferenceBoolean {

	@Override
	public PreferenceKey getKey() {
		return PreferenceKey.bGui;
	}

	@Override
	public String getDefaultValue() {
		return "false";
	}
}

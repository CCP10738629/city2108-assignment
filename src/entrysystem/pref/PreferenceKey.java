package entrysystem.pref;

/**
 * INI style preference keys, indicating the expected variable type and what the key is for.
 * <p>
 * Prefixes
 * b: boolean
 * i: integer
 * f: float
 * s: string
 */
public enum PreferenceKey {
	bGui,
	bSaveDataOnChange
}
package entrysystem.pref;

/**
 * Boolean indicating whether or not to overwrite data files automatically after adding/changing ID card/rooms.
 */
final class bSaveDataOnChange implements PreferenceBoolean {

	@Override
	public PreferenceKey getKey() {
		return PreferenceKey.bSaveDataOnChange;
	}

	@Override
	public String getDefaultValue() {
		return "true";
	}
}

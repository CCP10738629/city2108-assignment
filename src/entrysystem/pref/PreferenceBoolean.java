package entrysystem.pref;

/**
 * The interface boolean preferences are derived from. This interface implements Preference.
 */
interface PreferenceBoolean extends Preference {
	@Override
	default boolean check(String value) {
		switch (value.toLowerCase()) {
			case "true":
			case "false":
				return true;
			default:
				return false;
		}
	}
}

package entrysystem.pref;

/**
 * The interface all Preference classes are derived from.
 */
interface Preference {
	/**
	 * Checks if a given value is valid for the preference.
	 *
	 * @param value a string
	 * @return true if the value is valid, false if not
	 */
	boolean check(String value);

	/**
	 * Gets default value.
	 *
	 * @return the default value of the preference
	 */
	String getDefaultValue();

	/**
	 * Gets key.
	 *
	 * @return the associated PreferenceKey enum for the preference
	 */
	PreferenceKey getKey();
}

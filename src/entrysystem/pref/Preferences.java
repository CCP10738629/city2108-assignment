package entrysystem.pref;

import entrysystem.log.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

/**
 * Facade class. Interface for the Preference package. Contains methods for accessing and modifying preferences within the program. Aims to eliminate user/programmer error.
 */
public final class Preferences { // final to prevent child classes

	private static Preferences instance = null;

	private final Properties propertiesTable = new Properties();
	private final File PREFS_FILE = new File("preferences.txt");

	private Preferences() { // private to prevent external classes from making an instance
		Log.programLog.append("Preferences file: " + PREFS_FILE.getAbsolutePath());
		load();
		save();
	}

	/**
	 * Gets the singleton instance.
	 *
	 * @return the singleton instance
	 */
	public static Preferences getInstance() {
		if (instance == null)
			instance = new Preferences();
		return instance;
	}

	/**
	 * Load from preferences file if one exists. Loads defaults for preferences that are not present.
	 */
	public void load() {
		propertiesTable.clear();
		restoreDefaultAll();
		try {
			FileInputStream input = new FileInputStream(PREFS_FILE);
			propertiesTable.load(input);
			input.close();
			Log.programLog.append("Found preferences: " + propertiesTable);
		} catch (Exception e) {
			Log.programLog.append("Preferences file doesn't exist or couldn't be read. Continuing with defaults.");
		}
	}

	/**
	 * Restore the default value for a given preference.
	 *
	 * @param key PreferenceKey enum
	 */
	public void restoreDefault(PreferenceKey key) {
		propertiesTable.setProperty(key.name(), new PreferenceFactory().getPreference(key).getDefaultValue());
	}
	private void restoreDefault(Preference preference) {
		propertiesTable.setProperty(preference.getKey().name(), preference.getDefaultValue());
	}

	/**
	 * Restore the default value for all preferences.
	 */
	public void restoreDefaultAll() {
		for (Preference preference : new PreferenceFactory().getAllPreferences())
			restoreDefault(preference);
	}

	/**
	 * Save to preferences file, overwriting if necessary.
	 */
	public void save() {
		try {
			FileOutputStream output = new FileOutputStream(PREFS_FILE);
			propertiesTable.store(output, "College Entry System Preferences");
			output.close();
			Log.programLog.append("Saved preferences" + ": " + propertiesTable);
		} catch (Exception e) {
			Log.programLog.append("Error saving preferences.");
		}
	}

	/**
	 * Get the current value for a given preference.
	 *
	 * @param key PreferenceKey enum
	 * @return current preference value
	 */
	public String getValue(PreferenceKey key) {
		String value = propertiesTable.getProperty(key.name());
		Preference preference = new PreferenceFactory().getPreference(key);
		if (preference.check(value))
			return value;
		else
			restoreDefault(preference);
		return preference.getDefaultValue();
	}

	/**
	 * Sets a new value for a given preference.
	 *
	 * @param key   PreferenceKey enum
	 * @param value new preference value
	 */
	public void setValue(PreferenceKey key, String value) {
		if (new PreferenceFactory().getPreference(key).check(value))
			propertiesTable.setProperty(key.name(), value);
		if (Boolean.parseBoolean(getValue(PreferenceKey.bSaveDataOnChange)))
			save();
	}
}